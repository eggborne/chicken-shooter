class Element

  attr_accessor :hp,
                :gold,
                :max_hp,
                :rank,
                :base_size,
                :size,
				:base_size,
                :base_color,
                :color,
                :border_size,
                :angle,
                :x,
                :y,
                :sight_distance,
                :state,
                :speed,
                :slow,
				:sight
                
  def onscreen?
    @x.between?(1, $width + width/2) and @y.between?(1,$height + height/2)
  end
  
  def inside_frame?(size)
    @x.between?(size, $width + width/2 - size) and @y.between?(size, $height + height/2 - size)
  end
  
  def width
    self.class.image[:width] * size
  end
  
  def height
    self.class.image[:height] * size
  end
  
  def nearby?(object,distance)
    actual_distance_from_x_y(object.x,object.y) - (object.width/2).round <= distance
  end
  
  def near_point?(x,y,distance)
    actual_distance_from_x_y(x,y) <= distance
  end
  
  def undamaged?
    @hp == @max_hp
  end
  
  
  
  def touching?(object)
    horiz_dist = (object.width/2 + width/2)
    vert_dist = (object.height/2 + height/2)
    if @x.between?(object.x - horiz_dist, object.x + horiz_dist) \
    and @y.between?(object.y - vert_dist, object.y + vert_dist)
      true
    else
      false
    end
  end
  
  def heal(ally,amount)
    under_max = ally.max_hp - ally.hp
    if under_max <= amount
      ally.hp += under_max
    else
      ally.hp += amount
    end

  end
  
  
  def spawn_outside
    radius_x = (width/2).round
    radius_y = (height/2).round
    buffer_x = ($width/$spawn_buffer).round
    buffer_y = ($height/$spawn_buffer).round
    if rand.round == 1
      if rand.round == 1
        @x = rand(-buffer_x..-radius_x)
      else
        @x = rand($width + radius_x.. $width + buffer_x)
      end
      @y = rand(-buffer_y..$height + buffer_y)
    else
      if rand.round == 1
        @y = rand(-buffer_y..-radius_y)
      else
        @y = rand($height + radius_y.. $height + buffer_y)
      end
      @x = rand(-buffer_x..$width + buffer_x)
    end
    @hp = @max_hp
    @slow = 1
    @angle = 0
    @dying = false
    @dead_counter = 30
    @size = @base_size + (@rank*0.05)
  end
  
  def seek_player_ship
    step_straight_to($player_ship.x,$player_ship.y)
  end

  def step(direction,steps)
    case direction
      when :s
      @y += steps + @speed
      when :se
      @y += steps + @speed
      @x += steps + @speed
      when :sw
      @y += steps + @speed
      @x -= steps + @speed
      when :n
      @y -= steps + @speed
      when :ne
      @y -= steps + @speed
      @x += steps + @speed
      when :nw
      @y -= steps + @speed
      @x -= steps + @speed
      when :e
      @x += steps + @speed
      when :w
      @x -= steps + @speed
    end
  end
  
  def sight_distance
    @sight * @size
  end
    
  def actual_distance_from_x_y(x,y)
    Math.sqrt((x - @x)**2 + (y - @y)**2)
  end
      
  def direction_of_x_y(x,y)
    direction = $directions[rand(7)] if [x,y] == [@x,@y]
    if x > @x
      horizontal = :e
    elsif x < @x
      horizontal = :w
    end
    if y > @y
      vertical = :s
    elsif y < @y
      vertical = :n
    end
    direction = "#{vertical.to_s}#{horizontal.to_s}".to_sym
  end
  
  def step_away_from(x,y)
    if [@x,y] == [x,y]
      step($directions[rand($directions.length)],1)
    end
    step(opposite_direction(direction_of_x_y(x,y)),1)
  end

  def step_anywhere_but_toward(x,y)
    direction_index = $directions.index(direction_of_x_y(x,y))
    if [x,y] == [@x,@y]
      step($directions[rand($directions.length)],1)
    else
    ruled_out = $directions.slice(direction_index-1..direction_index+1)
    choices = $directions - ruled_out
    step(choices[rand(choices.length)],1)
    end
  end
  
  def step_precise(angle)
    self.x = point_at_angle(self.x,self.y, deg_to_rad(angle), 1)[0]
    self.y = point_at_angle(self.x,self.y, deg_to_rad(angle), 1)[1]
  end

  def step_straight_to(x,y)
    direction = direction_of_x_y(x,y)
    if direction.to_s.length == 1 or (x - @x).abs == (y - @y).abs
      step(direction.to_sym,1)
    else
      diff_x = (x - @x).abs
      diff_y = (y - @y).abs
      if diff_x > diff_y
        primary = direction
        secondary = direction.to_s[1].to_sym
        percent_more = percent_of_x_y_is(diff_x,diff_y)
      else
        primary = direction
        secondary = direction.to_s[0].to_sym
        percent_more = percent_of_x_y_is(diff_y,diff_x)
      end
      if rand(100) >= (100 - percent_more)
        step(primary,1)
      else
        step(secondary,1)
      end
    end
  end
  
  
end