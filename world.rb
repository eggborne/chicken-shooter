def generate(species,num)
  num.times { species.new.spawn_outside }
end

def opposite_direction(direction)
  if $directions.index(direction).nil?
    :n
  else
    $directions[$directions.index(direction)-4]
  end
end

def actual_distance_between(start_x,start_y,goal_x,goal_y)
  Math.sqrt((goal_x - start_x)**2 + (goal_y - start_y)**2)
end

def spin_out(object)
  object.angle += 12
  object.size -= 0.1
end

def percent_of_x_y_is(x,y)
  if x == 0
    return 0
  elsif x == y
    return 100
  else
    return (y*1000 / x*1000).to_s[0..1].to_i
  end
end

def actual_percent_of_x_y_is(x,y)
  (y/x).to_f
end

def living_creatures
  $population.values.flatten.select { |creature| creature.size > 0.1 and !creature.operating and creature.class != Chicken }
end

def dying_creatures
  $population.values.flatten.select { |creature| creature.dying }
end


def angle_of_point_a_b_from_x_y(a,b,x,y)
  rad_to_deg(Math.atan2(b-y,a-x)) 
end

def point_at_angle(x,y,angle,distance)
  goal_x = x+distance*Math.cos(angle)
  goal_y = y+distance*Math.sin(angle)
  coords = [goal_x,goal_y]
end

def deg_to_rad(deg)
  deg*Math::PI / 180
end
  
def rad_to_deg(rad)
  rad*180 / Math::PI
end

def operators
  collection = Hash.new
  $turrets.select {|t| t.manned }.each do |t|
    collection[t] = Array.new if !collection[t]
    collection[t] << t.operator
  end
  collection
end
 
class Turret < Element

  attr_accessor :burst_rate,
                :fire_rate,
                :fire_timer,
                :x,
                :y,
                :operative,
                :active_projectiles,
                :projectile,
                :sight_distance,
                :limit,
                :range,
                :angle,
				:idle_angle,
                :position,
                :offset,
                :manned,
                :locked,
                :target,
                :swivel_speed,
                :accuracy,
                :operator
                  
  def initialize(position,projectile)
    $turrets << self
    $projectiles_onscreen[self] = Array.new
    @operative = true
    @range = 800
    @active_projectiles = Array.new
    @projectile = projectile.new
    @projectile.turret = self
    @burst_rate = 1
    @fire_timer = 0
    @fire_rate = 0.4
    @sight_distance = 250
    if position == :left_cannon or position == :right_cannon
      @size = $sprite_size + 0.4
    else
      @size = $sprite_size - 0.4
    end
    @position = position
    @manned = false
    if @position == :center
	  @idle_angle = 0
      @fire_rate = 0.4
    elsif @position == :left_cannon
      @idle_angle = -90
      @fire_rate = 0.1
    elsif @position == :right_cannon
      @idle_angle = 90
      @fire_rate = 0.1
    elsif @position == :left
      @idle_angle = -2
      @fire_rate = 0.4
    elsif @position == :right
      @idle_angle = 2
      @fire_rate = 0.4
    elsif @position == :left_rear
      @idle_angle = 176
	  @fire_rate = 0.7
    elsif @position == :right_rear
      @idle_angle = 184
	  @fire_rate = 0.7
    end
	@offset = @idle_angle
    @locked = false
    @target = nil
    @swivel_speed = 4
    @accuracy = 1
    
    @operator = nil
    
    @color = 0xffffffff
    
  end
  
  def angle
    $player_ship_angle + @offset
  end
  
  
  def operator_x
    if @position == :right
      point_at_angle($player_ship.x,$player_ship.y, deg_to_rad($player_ship_angle+55), (42.0/$player_ship.class.image[:height].to_f)*$player_ship.class.image[:height])[0]
    elsif @position == :left
      point_at_angle($player_ship.x,$player_ship.y, deg_to_rad($player_ship_angle-55), 42)[0]
    elsif @position == :right_rear
      point_at_angle($player_ship.x,$player_ship.y, deg_to_rad($player_ship_angle+120), 42)[0]
    elsif @position == :left_rear
      point_at_angle($player_ship.x,$player_ship.y, deg_to_rad($player_ship_angle-120), 42)[0]
    end
  end
  
  def operator_y  
    if @position == :right
      point_at_angle($player_ship.x,$player_ship.y, deg_to_rad($player_ship_angle+55), (42.0/$player_ship.class.image[:height].to_f)*$player_ship.class.image[:height])[1]
    elsif @position == :left
      point_at_angle($player_ship.x,$player_ship.y, deg_to_rad($player_ship_angle-55), 42)[1]
    elsif @position == :right_rear
      point_at_angle($player_ship.x,$player_ship.y, deg_to_rad($player_ship_angle+120), 42)[1]
    elsif @position == :left_rear
      point_at_angle($player_ship.x,$player_ship.y, deg_to_rad($player_ship_angle-120), 42)[1]
    end
  end
   


  def x
    if @position == :center
      point_at_angle($player_ship.x,$player_ship.y, deg_to_rad($player_ship_angle), (45.0*$sprite_size/$player_ship.class.image[:height])*$player_ship.class.image[:height])[0]
    elsif @position == :right
      point_at_angle($player_ship.x,$player_ship.y, deg_to_rad($player_ship_angle+44), (52.0*$sprite_size/$player_ship.class.image[:height])*$player_ship.class.image[:height])[0]
    elsif @position == :left
      point_at_angle($player_ship.x,$player_ship.y, deg_to_rad($player_ship_angle-44), (52.0*$sprite_size/$player_ship.class.image[:height])*$player_ship.class.image[:height])[0]
    elsif @position == :right_rear
      point_at_angle($player_ship.x,$player_ship.y, deg_to_rad($player_ship_angle+122), (48.0*$sprite_size/$player_ship.class.image[:height])*$player_ship.class.image[:height])[0]
    elsif @position == :left_rear
      point_at_angle($player_ship.x,$player_ship.y, deg_to_rad($player_ship_angle-122), (48.0*$sprite_size/$player_ship.class.image[:height])*$player_ship.class.image[:height])[0]
    elsif @position == :left_cannon
        point_at_angle($player_ship.x,$player_ship.y, deg_to_rad($player_ship_angle-85), (42.0*$sprite_size/$player_ship.class.image[:height])*$player_ship.class.image[:height])[0]
    elsif @position == :right_cannon
        point_at_angle($player_ship.x,$player_ship.y, deg_to_rad($player_ship_angle+85), (42.0*$sprite_size/$player_ship.class.image[:height])*$player_ship.class.image[:height])[0]
    end
  end
  
  
  def y
    if @position == :center
      point_at_angle($player_ship.x,$player_ship.y, deg_to_rad($player_ship_angle), (45.0*$sprite_size/$player_ship.class.image[:height])*$player_ship.class.image[:height])[1]
    elsif @position == :right
      point_at_angle($player_ship.x,$player_ship.y, deg_to_rad($player_ship_angle+44), (53.0*$sprite_size/$player_ship.class.image[:height])*$player_ship.class.image[:height])[1]
    elsif @position == :left
      point_at_angle($player_ship.x,$player_ship.y, deg_to_rad($player_ship_angle-44), (53.0*$sprite_size/$player_ship.class.image[:height])*$player_ship.class.image[:height])[1]
    elsif @position == :right_rear
      point_at_angle($player_ship.x,$player_ship.y, deg_to_rad($player_ship_angle+122), (48.0*$sprite_size/$player_ship.class.image[:height])*$player_ship.class.image[:height])[1]
    elsif @position == :left_rear
      point_at_angle($player_ship.x,$player_ship.y, deg_to_rad($player_ship_angle-122), (48.0*$sprite_size/$player_ship.class.image[:height])*$player_ship.class.image[:height])[1]
    elsif @position == :left_cannon
        point_at_angle($player_ship.x,$player_ship.y, deg_to_rad($player_ship_angle-85), (42.0*$sprite_size/$player_ship.class.image[:height])*$player_ship.class.image[:height])[1]
    elsif @position == :right_cannon
        point_at_angle($player_ship.x,$player_ship.y, deg_to_rad($player_ship_angle+85), (42.0*$sprite_size/$player_ship.class.image[:height])*$player_ship.class.image[:height])[1]
    end
  end
  
  def fire_if_ready
    if @fire_timer == 0
      if $projectiles_onscreen.values.flatten.length < limit     
        @burst_rate.times do
          $projectiles_onscreen[self] = Array.new if !$projectiles_onscreen[self] 
          $projectiles_onscreen[self] << @projectile.clone
          if @projectile.class == IceBeam
            $projectiles_onscreen[self][-1].angle = $player_ship_angle
          else
            $projectiles_onscreen[self][-1].angle = rand(360)
          end
          $projectiles_onscreen[self][-1].fire_angle = angle
          $projectiles_onscreen[self][-1].x = x
          $projectiles_onscreen[self][-1].y = y
          if @projectile.class == Bait
            $projectiles_onscreen[self][-1].poison = true if rand(10) < 2
            $projectiles_onscreen[self][-1].target_x = $window.cursor_x
            $projectiles_onscreen[self][-1].target_y = $window.cursor_y
          end
          @fire_timer = @fire_rate * 20
        end
      end
    else
      @fire_timer -= 1
    end   
  end
  
  def able_to_shoot?(target)
    goal_angle = angle_of_point_a_b_from_x_y(target.x,target.y,x,y) - $player_ship_angle
    if target.class != Chicken and (@position == :right and goal_angle.between?(-24,84)) or (@position == :left and goal_angle.between?(-84,24)) or (@position == :right_rear and goal_angle.between?(10,180)) or (@position == :left_rear and goal_angle.between?(120,350))
      true
    else
      false
    end
  end 
  
  def swivel_to_aim_at(target)
    if target.class.superclass == Creature
    goal_angle = angle_of_point_a_b_from_x_y(target.x,target.y,x,y)
    difference = (goal_angle - angle)
      if difference.abs >= 5
        @offset += @swivel_speed if angle < goal_angle
        @offset -= @swivel_speed if angle > goal_angle
      else
        @offset += difference
        @target = target
      end  
    else
      difference = target - angle
      if difference.abs >= 5
        @offset += @swivel_speed if angle < target
        @offset -= @swivel_speed if angle > target
      else
        @offset += difference
      end
    end
  end
  
  def limit
    30
  end
  
  def width
    self.class.image[:width]
  end
  
  def height
    self.class.image[:height]
  end
  
  def self::image
    { normal: Pathname($directory).join('assets', 'turret.png').to_s,
      cannon: Pathname($directory).join('assets', 'cannon_head.png').to_s,
      width: 29,
      height: 34
    }
  end
  
end


  


class ShipBody < Element
  
  def initialize
    super
    @x = $width/2
    @y = $height/2
    @height = 143
    @width = 143
    @size = $sprite_size
    @speed = 5
    @color = 0xffffffff
  end
  
  def width
    self.class.image[:width] * @size
  end  
  
  def height
    self.class.image[:height] * @size
  end
  
  def self::image
    { normal: Pathname($directory).join('assets', 'falcon4.png').to_s,
      width: 113,
      height: 88
    }
  end
  
end

class Hull < Element
  attr_accessor :x,
                :y,
                :angle,
                :hp,
                :max_hp,
				:start_hp

  def initialize
    @max_hp = 1000
    @hp = @max_hp
	@start_hp = @max_hp
  end
  
  def x
    point_at_angle($player_ship.x,$player_ship.y,180,1)[0]
  end
  
  def y
    point_at_angle($player_ship.x,$player_ship.y,180,1)[1]
  end
  
  def angle
    $player_ship_angle
  end
  
  def self::image
    { 
      normal: Pathname($directory).join('assets', 'hull.png').to_s,
      hurt2: Pathname($directory).join('assets', 'hull2.png').to_s,
      hurt3: Pathname($directory).join('assets', 'hull3.png').to_s,
      hurt4: Pathname($directory).join('assets', 'hull4.png').to_s,
      hurt5: Pathname($directory).join('assets', 'hull5.png').to_s,
      hurt6: Pathname($directory).join('assets', 'hull6.png').to_s,
      hurt7: Pathname($directory).join('assets', 'hull7.png').to_s,
      hurt8: Pathname($directory).join('assets', 'hull8.png').to_s,
      width: 64,
      height: 64
    }
  end
end