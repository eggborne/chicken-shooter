

$orbs.values.flatten.each do |orb|

      @orb_image.draw_rot(orb.x, orb.y, 1, orb.angle, center_x = 0.5, center_y = 0.5, factor_x = orb.size, factor_y = orb.size, color = 0xffffffff, mode = :default )
	  draw_quad(  \
	  (orb.x + (1-orb.active_projectiles.length.to_f/orb.limit.to_f)*orb.width/2), orb.y-(orb.height/2 + (5+orb.width/10).ceil), 0xffffffff, \
	  (orb.x - (1-orb.active_projectiles.length.to_f/orb.limit.to_f)*orb.width/2), orb.y-(orb.height/2 + (5+orb.width/10).ceil), 0xffffffff, \
	  (orb.x + (1-orb.active_projectiles.length.to_f/orb.limit.to_f)*orb.width/2), orb.y-(orb.height/2 + 5), 0xffffffff, \
	  (orb.x - (1-orb.active_projectiles.length.to_f/orb.limit.to_f)*orb.width/2), orb.y-(orb.height/2 + 5), 0xffffffff, z = 2, mode = :default ) 

	end





class CannonBarrel < Element
    attr_accessor :burst_rate,
	              :fire_rate,
				  :magazine,
				  :fire_timer,
				  :x,
				  :y,
				  :operative,
				  :active_projectiles,
				  :projectile,
				  :sight_distance,
				  :limit
				  
  
  def initialize(projectile)
    $orbs[projectile] = Array.new unless $orbs[projectile]
	$orbs[projectile] << self
	@operative = true
	@active_projectiles = Array.new
	@burst_rate = 1
	@fire_rate = 0.3
	@fire_timer = 0
	@sight_distance = 120
	@projectile = projectile.new
	@projectile.orb = self
	@size = $sprite_size
	@angle = 0
  end
  
  def x
    $width/2
  end
  
  def y
    $height/2
  end
    
  
  def limit 
    8
  end
	

  

  def self::image
    { normal: Pathname($directory).join('assets', 'cannon.png').to_s,
	  width: 143,
	  height: 143
	}
  end
    

  
  def fire_if_ready
    if @fire_timer == 0
	  if projectiles_onscreen.length < limit
	    @burst_rate.times do
		  @active_projectiles << @projectile.clone
		  @active_projectiles[-1].x = rand(x-2..x+2).round
	      @active_projectiles[-1].y = rand(y-2..y+2).round
		  @fire_timer = @fire_rate * 20
	    end
	  end
	else
	  @fire_timer -= 1
	end
  end
    
  
  
  

  def upgrade(strike,speed,burst,rate,size)
    @projectile = @projectile.class.new
    @projectile.orb = self
	@projectile.strike += strike
	@projectile.size += size
	@projectile.speed += speed
    @fire_rate -= rate if fire_rate > 0
	@burst_rate += burst
  end

end
	
    
	