def either_dead(x,y)
  x.hp <= 0 or y.hp <= 0
end

def evaluate_battle(foe1,foe2)
  if foe1.hp <= 0
    killer = foe2 
    victim = foe1
  elsif foe2.hp <= 0
    killer = foe1
    victim = foe2
  end
  if victim.class == Chicken
    if !victim.operating.nil?
      victim.operating.manned = false
      victim.operating.operator = nil
      victim.operating = nil
      victim.dying = true
    else
      #$population[victim.class].delete(victim)
    end
  elsif victim.class.superclass == Projectile
  else
  
  
  if rand.round
    victim.promote
  end

# if victim.total("killed by") % 20 == 0
#   $population.delete(victim)
# end
  victim.dying = true
  victim.died += 1
#  $population[victim.class].delete(victim) if victim.died == 20
  end
end

def clash(one,two)
  one.take_turn(two)
  if either_dead(one,two)
    evaluate_battle(one,two)
  end
end

def top_performer(species,event)
  record = 0
  winner = living_creatures[-1]
  living_creatures.each do |creature|
    if creature.class == species
      current = creature.total(event)
      if current > record
        winner = creature
        record = current
      end
    end
  end
  return winner
end

def number_of_rank(what)
  count = 0
  $population.values.flatten.each do |k|
    count += 1 if k.rank == what
  end
  return count
end