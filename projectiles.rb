









class Bait < Projectile

  attr_accessor :since_fired,
                :target_x,
                :target_y,
                :poison
  
  def initialize
    super
    @angle = 0
    @since_fired = 0
    @strike = 150
    @speed = 0
    @size = $sprite_size
    @max_hp = 1000
    @hp = @max_hp
    @poison = false
  end
  
  
  def adjusted_size
    increment = @size/40
    @size - increment*((@max_hp - @hp)/35)
  end
  
  def spin_speed
#   if @since_fired.between?(1,600)
      speed = 20 - ((@since_fired/9))
      if speed < 0
        return 0
      else
        return speed
      end
#   else
#     return 0
#   end
  end
  
  def width
    self.class.image[:width] * @size
  end
  
  def height
    self.class.image[:height] * @size
  end

  
  def self::image
    { normal: Pathname($directory).join('assets', 'meat.png').to_s,
      width: 32,
      height: 16
    }
  end
  
  
  
end

class IceBeam < Projectile

  def initialize
    super
    @angle = 0
    @strike = 5
    @speed = 6
    @size = $sprite_size
    @slow = 0.5
    @duration = 10
    @base_size = @size
  end
  
  def color
    0xffb2b2ff
  end
  
  def spin_speed
    0
  end
  
  def width
    self.class.image[:width] * @size
  end
  
  def height
    self.class.image[:height] * @size
  end

  
  def self::image
    { normal: Pathname($directory).join('assets', 'ice_beam.png').to_s,
      width: 12,
      height: 12
    }
  end
  
end

class ZomBeam < Projectile

  def initialize
    super
    @angle = 0
    @strike = 5
    @speed = 16
    @size = $sprite_size + 20
    @slow = 0.5
    @duration = 10
    @base_size = @size
  end
  
  def color
    0xff00cc00
  end
  
  def spin_speed
    12
  end
  
  def width
    self.class.image[:width] * @size
  end
  
  def height
    self.class.image[:height] * @size
  end

  
  def self::image
    { normal: Pathname($directory).join('assets', 'pixel.bmp').to_s,
      width: 1,
      height: 1
    }
  end
  
end
  
class Fireball < Projectile

  attr_accessor :range
  
  def initialize
    super
    @angle = 0
    @idle_time = 0
    @strike = 500
    @speed = 10
    @size = $sprite_size + 1
    @range = 150
  end
  
  
  
  
  
  def spin_speed
    0
  end
  
  def width
    self.class.image[:width] * @size
  end
  
  def height
    self.class.image[:height] * @size
  end

  
  def self::image
    { normal: Pathname($directory).join('assets', 'fireball.png').to_s,
      width: 8,
      height: 8
    }
  end
  
  
  
end

class Saw < Projectile
  
  def initialize
    super
    @angle = 0
    @idle_time = 0
    @strike = 150
    @speed = 10
    @size = $sprite_size + 2
  end
  
  
  
  def spin_speed
    if @state == :just_fired
      12
    elsif @idle_time <= 0
      @idle_time = 0
      return 0
    elsif @idle_time.between?(1,180) 
      speed = (12 - (@idle_time/3).round)
      if speed < 0
        return 0
      else
        return speed
      end
    else
      return 0
    end
  end
  
  def width
    self.class.image[:width] * @size
  end
  
  def height
    self.class.image[:height] * @size
  end

  
  def self::image
    { normal: Pathname($directory).join('assets', 'saw.png').to_s,
      width: 8,
      height: 8
    }
  end
  
  
  
end

class Bullet < Projectile

  attr_accessor :base_size
  
  def initialize
    super
    @angle = 0
    @idle_time = 0
    @strike = 100
    @speed = 12
    @size = $sprite_size + 10
    @base_size = @size
  end
  
  
  
  def spin_speed2
    if @state == :just_fired
      12
    elsif @idle_time <= 0
      @idle_time = 0
      return 0
    elsif @idle_time.between?(1,180) 
      speed = (12 - (@idle_time/3).round)
      if speed < 0
        return 0
      else
        return speed
      end
    else
      return 0
    end
  end
  
  def spin_speed
    0
  end
  
  def width
    self.class.image[:width] * @size
  end
  
  def height
    self.class.image[:height] * @size
  end

  
  def self::image
    { normal: Pathname($directory).join('assets', 'pixel.bmp').to_s,
      width: 1,
      height: 1
    }
  end
  
  
  
end




  