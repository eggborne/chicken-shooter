require 'gosu'
require 'pathname'
require_relative 'global'
require_relative 'element_class'
require_relative 'projectile_class'
require_relative 'projectiles'
require_relative 'creature_class'
require_relative 'creatures'
require_relative 'battle'
require_relative 'world'


class GameWindow < Gosu::Window


  
  def initialize
    super($width, $height, $fullscreen)
    self.caption = "Chicken Protector"
    @chicken_image = Gosu::Image.new(self, Chicken.image[:normal], false)
    @frog_image = Gosu::Image.new(self, Frog.image[:normal], false)
    @squirrel_image = Gosu::Image.new(self, Squirrel.image[:normal], false)
    @bee_image = Gosu::Image.new(self, Bee.image[:normal], false)
    @demon_image = Gosu::Image.new(self, Demon.image[:normal], false)
    @saw_image = Gosu::Image.new(self, Saw.image[:normal], false)
    @fireball_image = Gosu::Image.new(self, Fireball.image[:normal], false)
    @ice_beam_image = Gosu::Image.new(self, IceBeam.image[:normal], false)
    @bait_image = Gosu::Image.new(self, Bait.image[:normal], false)
    @bullet_image = Gosu::Image.new(self, Bullet.image[:normal], false)
    @background_image = Gosu::Image.new(self, Pathname($directory).join('assets', 'bg.jpg').to_s, false)
    @orb_image = Gosu::Image.new(self, Pathname($directory).join('assets', 'orb.png').to_s, false)
    @pointer_image = Gosu::Image.new(self, Pathname($directory).join('assets', 'pointer.png').to_s, false)
    @ship_body_image = Gosu::Image.new(self, ShipBody.image[:normal], false)
    @hull_image = Gosu::Image.new(self, Hull.image[:normal], false)
    @hull_image2 = Gosu::Image.new(self, Hull.image[:hurt2], false)
    @hull_image3 = Gosu::Image.new(self, Hull.image[:hurt3], false)
    @hull_image4 = Gosu::Image.new(self, Hull.image[:hurt4], false)
    @hull_image5 = Gosu::Image.new(self, Hull.image[:hurt5], false)
    @hull_image6 = Gosu::Image.new(self, Hull.image[:hurt6], false)
    @hull_image7 = Gosu::Image.new(self, Hull.image[:hurt7], false)
    @hull_image8 = Gosu::Image.new(self, Hull.image[:hurt8], false)
    @turret_image = Gosu::Image.new(self, Turret.image[:normal], false)
    @cannon_head_image = Gosu::Image.new(self, Turret.image[:cannon], false)
  end
  
  def cursor_x
    mouse_x
  end
  
  def cursor_y
    mouse_y
  end
  
  def button_down(id)
    case id 
    when Gosu::KbEscape
    close
    when Gosu::KbL
    if $lifebars
      $lifebars = false
    else
      $lifebars = true
    end
    
    end
  end
  
  def update
    
    if $hull.hp <= 0
    # GAME OVER
    else
	
    $hull.start_hp = $hull.hp
	
    $player_ship_angle = angle_of_point_a_b_from_x_y(mouse_x,mouse_y,$player_ship.x,$player_ship.y)
	
    if button_down?(Gosu::MsLeft)
      $center_turret.fire_if_ready
    elsif button_down?(Gosu::KbSpace)
      $left_cannon.fire_if_ready
      $right_cannon.fire_if_ready
    end
    
    $turrets.select { |t| t.manned }.each do |t|
      t.target = nil unless t.target and (t.target.onscreen? or t.able_to_shoot?(t.target))
      living_creatures.select {|c| c.onscreen? and !c.dying and c.nearby?(t,t.sight_distance) and t.able_to_shoot?(c) }.each do |c|
        t.swivel_to_aim_at(c)
        t.locked = true
        if t.target == c
          t.fire_if_ready
        end
      end
      if t.target and (t.target.hp <= 0 or !t.target.nearby?(t,t.sight_distance) or !t.able_to_shoot?(t.target))
        t.locked = false
      end
      unless t.locked
        t.swivel_to_aim_at($player_ship_angle + t.idle_angle)
      end
    end
      

  
    
    $projectiles_onscreen.values.flatten.each do |p|
	
      if !p.onscreen? or (p.class == Fireball and !p.nearby?($player_ship,p.range))
        $projectiles_onscreen[p.turret].delete(p)
      end
	  
      if p.class == Fireball
        grow(p,0.3) unless p.size > $sprite_size *5
      end
	  
      moved = false
      living_creatures.select {|c| c.onscreen? and !c.dying and c.class != Chicken }.each do |k|
        if p.touching?(k) 
          if (p.class == Saw or p.class == Bullet or p.class == Fireball)
            k.step_away_from($player_ship.x,$player_ship.y)
            k.step_away_from($player_ship.x,$player_ship.y)
            $projectiles_onscreen[p.turret].delete(p)
          elsif p.class == IceBeam
            k.slow += p.slow unless k.slow == 10
          elsif p.class == ZomBeam
            k.zombie = true
            $projectiles_onscreen[p.turret].delete(p)
          end
          k.hp -= p.strike
          if either_dead(p,k)
		    $score += $ranks.index(k.rank)*5
            evaluate_battle(p,k) 
          end
        end
      end
      if !moved
          spin(p,p.spin_speed) unless p.spin_speed.zero?
          p.since_fired += 1 if p.class == Bait
          if p.class == Saw or p.class == Bullet or p.class == Fireball or p.class == IceBeam or p.class == ZomBeam
            p.speed.times do
              p.step_precise(p.fire_angle)                     
            end                    
          end
      end
    end
  

	  
    dying_creatures.each do |d|
      d.hp = 0 if d.hp != 0
      if d.dead_counter > 0
        spin_out(d)
        d.dead_counter -= 1
      else
        d.spawn_outside
        d.state = :seeking_player_ship
      end
    end
    
    operators.values.flatten.each do |o|
      o.x = o.operating.operator_x
      o.y = o.operating.operator_y
      o.angle = o.operating.angle + 180
    end
    
    living_creatures.select {|c| !c.dying and c.class != Chicken }.each do |k|
    
      moved = false
      
      unless k.inside_frame?(20)
        k.seek_player_ship 
      else
	  
        (living_creatures.reject {|creature| creature == k } + $turrets + $ships + $projectiles_onscreen.values.flatten).each do |m|

            if k.touching?(m)
              if k.class == m.class
                if k.state == :zombie
                  m.hp -= k.strike  
                  m.zombie = true if rand.round
                else
                k.heal(m,k.heal_rate) if m.wounded? and !k.wounded?
                k.step_anywhere_but_toward(m.x,m.y)
                end
              else
                if m.class == ShipBody and k.class != Chicken
                  $hull.hp -= 1
                  while k.touching?(m)
                    k.step_anywhere_but_toward(m.x,m.y) if rand(3) < 1
                    k.step_anywhere_but_toward(m.x,m.y) if rand(3) < 1
                  end
                elsif m.class == Turret
                  if m.manned
                    m.operator.hp -= k.strike 
                    evaluate_battle(k,m.operator) if m.operator.hp <= 0
                  end
                  k.step_anywhere_but_toward(m.x,m.y)
                
                else
                  k + m if m.state != :zombie and m.class.superclass != Projectile and m.class != ShipBody and m.class != Turret 
                end
              end
              k.step_away_from(m.x,m.y)
              moved = true
            end
          end
        if (!moved or k.target == nil) and rand(k.slow) <= 1
          k.seek_player_ship
        end
      end
    end
    
    
    

    

        if $hull.hp < $hull.start_hp
          $player_ship.color = 0xffff0000
        else
          $player_ship.color = 0xffffffff
        end

    
    
    end
     
  end
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  def draw
  
  
    if $hull.hp <= 0
      Gosu::Image.from_text(self,"YOU LOSE","Arial",60).draw(250,100,3)
      Gosu::Image.from_text(self,"Score: #{$score}","Arial",40).draw(250,200,3)
    else
  
  
    $turrets.each do |t|
      if t.position == :left_cannon or t.position == :right_cannon
        @cannon_head_image.draw_rot(t.x, t.y, 6, t.angle, center_x = 0.5, center_y = 0.5, factor_x = t.size, factor_y = t.size)
      else
        @turret_image.draw_rot(t.x, t.y, 6, t.angle, center_x = 0.5, center_y = 0.5, factor_x = t.size, factor_y = t.size,  color = t.color) unless t.position != :center and !t.manned
      end
    end
  
    
    
    
    
    
#    @turret_image.draw_rot($turrets[0].x, $turrets[0].y, 5, $player_ship_angle) #, center_x = 0.5, center_y = 0.5, factor_x = 0.5, factor_y = 0.5)
    @ship_body_image.draw_rot($player_ship.x, $player_ship.y, 5, $player_ship_angle, center_x = 0.35, center_y = 0.5, factor_x = $player_ship.size, factor_y = $player_ship.size, color = $player_ship.color)
    
    if $hull.hp == $hull.max_hp
      @hull_image.draw_rot($player_ship.x, $player_ship.y, 5, $player_ship_angle, center_x = 0.5, center_y = 0.5, factor_x = $player_ship.size, factor_y = $player_ship.size)
    elsif $hull.hp.between?(800,1000)
      @hull_image2.draw_rot($player_ship.x, $player_ship.y, 5, $player_ship_angle, center_x = 0.5, center_y = 0.5, factor_x = $player_ship.size, factor_y = $player_ship.size)
    elsif $hull.hp.between?(700,800)
      @hull_image3.draw_rot($player_ship.x, $player_ship.y, 5, $player_ship_angle, center_x = 0.5, center_y = 0.5, factor_x = $player_ship.size, factor_y = $player_ship.size)
    elsif $hull.hp.between?(600,700)
      @hull_image4.draw_rot($player_ship.x, $player_ship.y, 5, $player_ship_angle, center_x = 0.5, center_y = 0.5, factor_x = $player_ship.size, factor_y = $player_ship.size)
    elsif $hull.hp.between?(500,600)
      @hull_image5.draw_rot($player_ship.x, $player_ship.y, 5, $player_ship_angle, center_x = 0.5, center_y = 0.5, factor_x = $player_ship.size, factor_y = $player_ship.size)
    elsif $hull.hp.between?(400,500)
      @hull_image6.draw_rot($player_ship.x, $player_ship.y, 5, $player_ship_angle, center_x = 0.5, center_y = 0.5, factor_x = $player_ship.size, factor_y = $player_ship.size)
    elsif $hull.hp.between?(300,400)
      @hull_image7.draw_rot($player_ship.x, $player_ship.y, 5, $player_ship_angle, center_x = 0.5, center_y = 0.5, factor_x = $player_ship.size, factor_y = $player_ship.size)
    elsif $hull.hp < 300
      @hull_image8.draw_rot($player_ship.x, $player_ship.y, 5, $player_ship_angle, center_x = 0.5, center_y = 0.5, factor_x = $player_ship.size, factor_y = $player_ship.size)
    end
    
	
	
    if button_down?(Gosu::KbW) and button_down?(Gosu::KbA)
      $player_ship.y -= $player_ship.speed unless $player_ship.y <= $player_ship.height/2
      $player_ship.x -= $player_ship.speed unless $player_ship.x <= $player_ship.width/2
    elsif button_down?(Gosu::KbW) and button_down?(Gosu::KbD)
      $player_ship.y -= $player_ship.speed unless $player_ship.y <= $player_ship.height/2
      $player_ship.x += $player_ship.speed unless $player_ship.x >= $width - $player_ship.width/2
    elsif button_down?(Gosu::KbW)
      $player_ship.y -= $player_ship.speed unless $player_ship.y <= $player_ship.height/2
    elsif button_down?(Gosu::KbA) and button_down?(Gosu::KbS)
      $player_ship.x -= $player_ship.speed unless $player_ship.x <= $player_ship.width/2
      $player_ship.y += $player_ship.speed unless $player_ship.y >= $height - $player_ship.height/2
    elsif button_down?(Gosu::KbA)
      $player_ship.x -= $player_ship.speed unless $player_ship.x <= $player_ship.width/2
    elsif button_down?(Gosu::KbS) and button_down?(Gosu::KbD)
      $player_ship.x += $player_ship.speed unless $player_ship.x >= $width - $player_ship.width/2
      $player_ship.y += $player_ship.speed unless $player_ship.y >= $height - $player_ship.height/2
    elsif button_down?(Gosu::KbS)
      $player_ship.y += $player_ship.speed unless $player_ship.y >= $height - $player_ship.height/2
    elsif button_down?(Gosu::KbD) and button_down?(Gosu::KbW)
      $player_ship.x += $player_ship.speed unless $player_ship.x >= $width - $player_ship.width/2
      $player_ship.y -= $player_ship.speed unless $player_ship.y <= $player_ship.width/2
    elsif button_down?(Gosu::KbD)
      $player_ship.x += $player_ship.speed unless $player_ship.x >= $width - $player_ship.width/2
    end
    if button_down?(Gosu::KbF)
      draw_triangle(point_at_angle($left_turret.x, $left_turret.y,$left_turret.angle,30)[0], point_at_angle($left_turret.x, $left_turret.y,$left_turret.angle,30)[1], 0x22ffff00, point_at_angle($left_turret.x, $left_turret.y,$left_turret.angle,30)[0], point_at_angle($left_turret.x, $left_turret.y,90+$left_turret.angle,30)[1], 0x22ffff00, mouse_x, mouse_y, 0x88ffff00, z = 4, mode = :additive)     
    end
    
    
    @background_image.draw_rot(self.width/2, self.height/2, 0, $bg_position) #, center_x = 0.5, center_y = 0.5, factor_x = 1.5, factor_y = 1.5, color = 0xffffffff, mode = :default )
    spin_background unless $spin_speed.zero?
    @pointer_image.draw(mouse_x,mouse_y,4)
    
    $projectiles_onscreen.values.flatten.each do |p|
      if p.class == Bait
        if p.poison
          @bait_image.draw_rot(p.x,p.y,4,p.angle, center_x = 0.5, center_y = 0.5, factor_x = p.adjusted_size, factor_y = p.adjusted_size, color = 0xff00cc00)
        else
          @bait_image.draw_rot(p.x,p.y,4,p.angle, center_x = 0.5, center_y = 0.5, factor_x = p.adjusted_size, factor_y = p.adjusted_size)
        end
      elsif p.class == Saw
        @saw_image.draw_rot(p.x,p.y,1,p.angle, center_x = 0.5, center_y = 0.5, factor_x = p.size, factor_y = p.size)
      elsif p.class == Bullet or p.class == ZomBeam
        @bullet_image.draw_rot(p.x,p.y,1,p.angle, center_x = 0.5, center_y = 0.5, factor_x = p.size, factor_y = p.size, color = p.color)
      elsif p.class == Fireball
        @fireball_image.draw_rot(p.x,p.y,1,p.angle, center_x = 0.5, center_y = 0.5, factor_x = p.size, factor_y = p.size, color = 0xffff9999)
      elsif p.class == IceBeam
        @ice_beam_image.draw_rot(p.x,p.y,1,p.angle, center_x = 0.5, center_y = 0.5, factor_x = p.size, factor_y = p.size, color = p.color)
      end
    end
    
    

    [living_creatures + operators.values].flatten.each do |k|
      
      if $lifebars and k.onscreen?
        draw_quad(  (k.x - (k.hp.to_f/k.max_hp.to_f)*k.width/2), k.y-(k.height/2 + 5+k.width/10).ceil, k.color(:lifebar), (k.x + (k.hp.to_f/k.max_hp.to_f)*k.width/2), k.y-(k.height/2 + 5+k.width/10).ceil, k.color(:lifebar), (k.x - (k.hp.to_f/k.max_hp.to_f)*k.width/2), k.y-(k.height/2 + 5).ceil, k.color(:lifebar), (k.x + (k.hp.to_f/k.max_hp.to_f)*k.width/2), k.y-(k.height/2 + 5), k.color(:lifebar), z = 5, mode = :default ) unless k.class == Chicken and k.undamaged?
      end

        
        case k.class.to_s
          when "Chicken"
          if !k.operating.nil?
            @chicken_image.draw_rot(k.x, k.y, 6, k.angle, center_x = 0.5, center_y = 0.5, factor_x = k.size+0.3, factor_y = k.size+0.3, color = k.color(:body), mode = :default )
          else
            @chicken_image.draw_rot(k.x, k.y, 1, k.angle, center_x = 0.5, center_y = 0.5, factor_x = k.size, factor_y = k.size, color = k.color(:body), mode = :default )
            if $hull.undamaged?
              @chicken_image.draw_rot(k.x, k.y, 1, k.angle, center_x = 0.5, center_y = 0.5, factor_x = k.size, factor_y = k.size, color = 0xffffff66, mode = :default )
            end
          end
        when "Frog"
          @frog_image.draw_rot(k.x, k.y, 3, k.angle, center_x = 0.5, center_y = 0.5, factor_x = k.size, factor_y = k.size, color = k.color(:body), mode = :default )
          if k.state == :zombie
            @frog_image.draw_rot(k.x, k.y, 3, k.angle, center_x = 0.5, center_y = 0.5, factor_x = k.size, factor_y = k.size, color = 0xff00cc00, mode = :default )
          end
        when "Squirrel"
          @squirrel_image.draw_rot(k.x, k.y, 3, k.angle, center_x = 0.5, center_y = 0.5, factor_x = k.size, factor_y = k.size, color = k.color(:body), mode = :default )
          if k.state == :zombie
            @squirrel_image.draw_rot(k.x, k.y, 3, k.angle, center_x = 0.5, center_y = 0.5, factor_x = k.size, factor_y = k.size, color = 0xff00cc00, mode = :default )
          end
        when "Bee"
          @bee_image.draw_rot(k.x, k.y, 3, k.angle, center_x = 0.5, center_y = 0.5, factor_x = k.size, factor_y = k.size, color = k.color(:body), mode = :default )
          if k.state == :zombie
            @bee_image.draw_rot(k.x, k.y, 3, k.angle, center_x = 0.5, center_y = 0.5, factor_x = k.size, factor_y = k.size, color = 0xff00cc00, mode = :default )
          end
        when "Demon"
          @demon_image.draw_rot(k.x, k.y, 3, k.angle, center_x = 0.5, center_y = 0.5, factor_x = k.size, factor_y = k.size, color = k.color(:body), mode = :default )
          if k.state == :zombie
            @bee_image.draw_rot(k.x, k.y, 3, k.angle, center_x = 0.5, center_y = 0.5, factor_x = k.size, factor_y = k.size, color = 0xff00cc00, mode = :default )
          end
        end
        
        end
        
        
        


      
    end
    
    
    

#   Gosu::Image.from_text(self,"Total creatures: #{$population.values.flatten.length}","Arial",18).draw(25,40,3)
#   if $charges[Chicken]
#      Gosu::Text(self, " Score: #{$charges[Chicken]}","Arial",24).draw($width-200,30,3)
#   else
#      Gosu::Text(self, " Score: 0","Arial",24).draw($width-200,30,3)
#   end
    
  end
  
end


$player_ship = ShipBody.new
$hull = Hull.new
$ships << $player_ship

7.times do
  Chicken.new
end

generate(Frog,2)
generate(Squirrel,3)
generate(Bee,6)

$center_turret = Turret.new(:center,Saw)
$left_turret = Turret.new(:left,Bullet)
$right_turret = Turret.new(:right,Bullet)
$left_rear_turret = Turret.new(:left_rear,Bullet)
$right_rear_turret = Turret.new(:right_rear,Bullet)
$left_cannon = Turret.new(:left_cannon,Fireball)
$right_cannon = Turret.new(:right_cannon,Fireball)

chicken1 = $population[Chicken][0]
chicken2 = $population[Chicken][1]
chicken3 = $population[Chicken][2]
chicken4 = $population[Chicken][3]

chicken1.man_turret($left_turret)
chicken2.man_turret($right_turret)
chicken3.man_turret($left_rear_turret)
chicken4.man_turret($right_rear_turret)

$window = GameWindow.new
$window.show

