class Creature < Element

  attr_accessor :bounty,
                :strike,
                :block,
                :heal_rate,
                :dodge_chance,
                :dead_counter,
                :dying,
                :color,
                :exp,
                :target,
                :operating,
				:died,
				:zombie
                 
  def initialize
    super
    $population[self.class] = Array.new unless $population[self.class]
    $population[self.class] << self
	@died = 0
    @dead_counter = 200
    @dying = false
    @interactions = Hash.new
    @outcomes = Hash.new
    @rank = 0
    @state = :seek_player_ship
    @size = $sprite_size
	@base_size = @size
    @exp = 0
    @target = nil
    @operating = nil
    @slow = 1
	@zombie = false
  end
  
  def bounty
    if total("killed by").zero?
      0
    else
      (total("killed him") / total("killed by")).round
    end
  end
  
  
  def absorb_charge
    if $charges.include?(self.class)
      $charges[self.class] += 1
    else
      $charges[self.class] = 1
    end
    if $charges[self.class] % 1000 == 0
      $spawn_buffer += 0.1 unless $spawn_buffer == 200
    end
  end
  
  def color(element)
    if element == :lifebar
    
    if @hp > @max_hp*0.8
      $damage_colors[element][6]
    elsif @hp > @max_hp*0.7 and @hp <= @max_hp*0.8
      $damage_colors[element][5]
    elsif @hp > @max_hp*0.6 and @hp <= @max_hp*0.7
      $damage_colors[element][4]
    elsif @hp > @max_hp*0.5 and @hp <= @max_hp*0.6
      $damage_colors[element][3]
    elsif @hp > @max_hp*0.4 and @hp <= @max_hp*0.5
      $damage_colors[element][2]
    elsif @hp > @max_hp*0.3 and @hp <= @max_hp*0.4
      $damage_colors[element][1]
    elsif @hp <= @max_hp*0.3
      $damage_colors[element][0]
    end
    
    elsif element == :body
    
    case @state
      when :slow
      0xff6666ff
      when :seeking_player_ship
      0xffffffff
      when :being_damaged
      0xffff0000
      when :chasing_wounded_teammate
      0xffffffff
      when :chasing_healer
      0xffffffff
      when :evading_stronger_enemy
      0xffffffff
#     when :eating_bait
#     0xff000000
#     when :chasing_bait
#     0xff0000ff
      else
      0xffffffff
    end
    
    end
    
  end
  
  
  
  
  
  
  
  
  
  
  
  
  def wounded?
    @hp < (@max_hp/3).round
  end
  
  def +(opp)
    clash(self,opp)
  end
  
  def dodge_now?
    true if rand(100) < dodge_chance
  end


  
  def outranks_x_by_at_least_y?(object,num)
    @rank >= object.rank + num
  end    
   
  def needs_healing_from?(friend)
    wounded? and !friend.wounded?
  end
    
  def needs_to_heal?(friend)
    (!wounded? and outranks_x_by_at_least_y?(friend,1) and friend.wounded?) \
    or (@rank > 0 and outranks_x_by_at_least_y?(friend,2) and !wounded? and friend.hp < friend.max_hp/2)
  end
    
  def take_turn(opp)
    if opp.dodge_now?
    else
      blocked = opp.block
      blow = @strike - blocked
      if blocked > blow
        blow = 0
      end
      if blow > opp.hp
        blow = opp.hp
      end
      opp.hp -= blow
    end
  end
    
  def promote
    unless @rank == $ranks[-1]
      @max_hp += rand(20..50)
      @strike += 2
      @rank += 1
      @speed += 0.1 if @rank % 3 == 0
    end   
  end

  def man_turret(turret)
    turret.operator = self
    turret.manned = true
    @x = turret.x
    @y = turret.y
    @operating = turret
  end
 
end





    
    
      