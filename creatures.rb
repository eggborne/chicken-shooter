
class Chicken < Creature
  
  def initialize
    super
    @gold = 0
    @strike = rand(2..3)    
    @block = 0
    @heal_rate = 20
    @dodge_chance = 7
    @max_hp = 1000  
    @hp = @max_hp
    @speed = 0
    @sight = 60
    @angle = 0
    @base_color = 0xff66ffff
    @x = rand($player_ship.x-$player_ship.height/2..$player_ship.x+$player_ship.height/2)
    @y = rand($player_ship.y-$player_ship.height/2..$player_ship.y+$player_ship.height/2)
    @size = $sprite_size
	@base_size = @size
  end

  def self::image
    { normal: Pathname($directory).join('assets', 'chicken.png').to_s,
      width: 16,
      height: 16
    }
  end

end

class Bee < Creature

  def initialize
    super
    @gold = 0
    @strike = rand(1..6).round  
    @block = 0
    @heal_rate = 10
    @dodge_chance = 15
    @max_hp = 150   
    @hp = @max_hp
    @speed = rand(0.3)
    @sight = 80
    @angle = 0
    @base_color = 0xffff9900
	@base_size = $sprite_size + 0.5

  end
  
  
  def self::image
    { normal: Pathname($directory).join('assets', 'bee.png').to_s,
      width: 16,
      height: 16
    }
  end
  
end

class Frog < Creature

  def initialize
    super
    @gold = 0
    @strike = rand(1..2).round  
    @block = 0
    @heal_rate = 20
    @dodge_chance = 7
    @max_hp = 400   
    @hp = @max_hp
    @speed = 0
    @sight = 60
    @angle = 0
    @base_color = 0xff009900
	@base_size = @size
  
  end
  
  def self::image
    { normal: Pathname($directory).join('assets', 'frog.png').to_s,
      width: 16,
      height: 16
    }
  end
  
end

class Squirrel < Creature

  def initialize
    super
    @gold = 0
    @strike = rand(1..2).round  
    @block = 0
    @heal_rate = 20
    @dodge_chance = 5
    @max_hp = 300   
    @hp = @max_hp
    @speed = rand(0.2)
    @sight = 60
    @angle = 0
    @base_color = 0xffb8704d
	@base_size = @size

  end
  
  def self::image
    { normal: Pathname($directory).join('assets', 'squirrel.png').to_s,
      width: 24,
      height: 20
    }
  end
  
end

class Demon < Creature

  def initialize
    super
    @gold = 0
    @strike = rand(10..20).round
    @block = 1
    @heal_rate = 20
    @crit_chance = 5
    @dodge_chance = 5
    @max_hp = 30000 
    @hp = @max_hp
    @speed = 0
    @sight = 60
    @angle = 0
    @size = $sprite_size/2
	@base_size = @size

  end

  
  def self::image
    { normal: Pathname($directory).join('assets', 'demon.png').to_s,
      width: 264,
      height: 192
    }
  end
end