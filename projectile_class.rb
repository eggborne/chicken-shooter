
class Projectile < Element
  attr_accessor :idle_time,
                :spin_speed,
                :strike,
                :hp,
                :size,
                :turret,
                :state,
                :range,
                :fire_angle
                

  def initialize
    super
    @hp = 1
    @speed = 0
    @size = $sprite_size * 1.5
    @state = :just_fired
    @fire_angle = $player_ship_angle
    @color = 0xffa6ed00
  end  
    
end
  
  
    