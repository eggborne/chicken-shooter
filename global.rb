$width = 800   
$height = 600
$fullscreen = false
$sprite_size = 1
$score = 0
$directory = Dir.pwd

$player_ship_angle = 0

$lifebars = true

$square_mode = false

$bg_position = 0
$spin_speed = 0
$spawn_buffer = 1
$charges = Hash.new
$ranks = Array(0..25)

$directions = [:n,:ne,:e,:se,:s,:sw,:w,:nw]

$damage_colors = { body: [0xffff4d4d,  0xffff8080, 0xffff9999, 0xffffb2b2, 0xffffcccc, 0xffffe6e6, 0xffffffff],
                   lifebar: [0xffff0000,  0xffff4000, 0xffffb200, 0xffffff00, 0xffa6ed00, 0xff4cdb00, 0xff00cc00]
                 }
                               
$population = Hash.new
$turrets = Array.new
$ships = Array.new
$projectiles_onscreen = Hash.new

def spin_background
  unless $spin_speed == 0
    diff = (360 - $bg_position).abs
    if $bg_position == 360
      $bg_position = $spin_speed
    elsif diff > 0 and diff < $spin_speed
      $bg_position = ($spin_speed) - diff
    else
      $bg_position += $spin_speed
    end
  end
end

def spin(object, speed)
  diff = (360 - object.angle).abs
  if object.angle == 360
    object.angle = speed
  elsif diff > 0 and diff < $spin_speed
    object.angle = (speed) - diff
  else
    object.angle += speed
  end
end
  
def grow(object,speed)
  object.size += speed
end    

